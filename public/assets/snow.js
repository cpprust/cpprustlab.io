const TICK_DURATION = 1000/60;
const MIN_SNOW_SPAWN_PER_SECOND = 0.4;
const MAX_SNOW_SPAWN_PER_SECOND = 0.6;
const CLICK_SNOW_SPAWN_COUNT = 10;
const MIN_SNOW_FORCE = 0.4;
const MAX_SNOW_FORCE = 2;
const MIN_SNOW_RADIUS = 2;
const MAX_SNOW_RADIUS = 8;
const MIN_SNOW_ALPHA = 0.1;
const MAX_SNOW_ALPHA = 0.3;
const SNOW_MELT_RATE = 0.0005;

const snowColor = {
    r: 255,
    g: 255,
    b: 255,
}

let snowArray = [];

class Snow {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.radius = MIN_SNOW_RADIUS + Math.random() * (MAX_SNOW_RADIUS - MIN_SNOW_RADIUS);
        this.force = {
            x: MIN_SNOW_FORCE + Math.random() * (MAX_SNOW_FORCE - MIN_SNOW_FORCE),
            y: MIN_SNOW_FORCE + Math.random() * (MAX_SNOW_FORCE - MIN_SNOW_FORCE),
        };
        this.alpha = MIN_SNOW_ALPHA + Math.random() * (MAX_SNOW_ALPHA - MIN_SNOW_ALPHA);
    }

    drop() {
        this.x += this.force.x;
        this.y += this.force.y;
    }

    melt() {
        if(this.alpha - SNOW_MELT_RATE > 0) {
            this.alpha -= SNOW_MELT_RATE;
        } else {
            this.alpha = 0;
        }
    }

    isMelted() {
        return this.alpha <= 0;
    }
}

function calcSnowSpawnCount() {
    return Math.round(MIN_SNOW_SPAWN_PER_SECOND + Math.random() * (MAX_SNOW_SPAWN_PER_SECOND - MIN_SNOW_SPAWN_PER_SECOND))
}

function calcRandomSnowSpawnPos(canvasWidth, canvasHeight) {
    if(Math.random() * canvasWidth >= Math.random() * canvasHeight) {
            return {
            x: Math.random() * canvasWidth - MAX_SNOW_RADIUS,
            y: 0 - MAX_SNOW_RADIUS,
        }
    } else {
        return {
            x: 0 - MAX_SNOW_RADIUS,
            y: Math.random() * canvasHeight,
        }
    }
}

function drawSnow(ctx, snow) {
    ctx.beginPath();
    ctx.fillStyle = `rgba(${snowColor.r}, ${snowColor.g}, ${snowColor.b}, ${snow.alpha})`;
    ctx.arc(snow.x, snow.y, snow.radius, 0, Math.PI * 2);
    ctx.fill();
}

function isSnowOutOfScene(snow, sceneWidth, sceneHeight) {
    if(sceneWidth <= snow.x - snow.radius) {
        return true;
    } else if(sceneHeight <= snow.y - snow.radius) {
        return true;
    }
    return false;
}

function isSnowOverSceneBottom(snow, sceneHeight) {
    if(sceneHeight < snow.y) {
        return true;
    }
    return false;
}

function attachCanvasOnElement(canvas, parentElement) {
    parentElement.appendChild(canvas);
    canvas.style.position = "absolute";
    canvas.style.left = "0";
    canvas.style.top = "0";
    canvas.style.zIndex = "-1";
    canvas.width = parentElement.offsetWidth;
    canvas.height = parentElement.offsetHeight;
}

function tick(sceneWidth, sceneHeight) {
    snowArray.forEach((snow, snowIndex) => {
        if(isSnowOverSceneBottom(snow, sceneHeight)) {
            snow.melt();
        } else {
            snow.drop();
        }
    });
    snowArray = snowArray.filter((snow) => 
        !(isSnowOutOfScene(snow, sceneWidth, sceneHeight) || snow.isMelted())
    );

    const snowSpawnCount = calcSnowSpawnCount();
    for(let i = 0; i < snowSpawnCount; ++i) {
        const spawnPos = calcRandomSnowSpawnPos(sceneWidth, sceneHeight);
        snowArray.push(new Snow(spawnPos.x, spawnPos.y));
    }
}

function render(canvas) {
    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    snowArray.forEach((snow) => drawSnow(ctx, snow));
}

addEventListener("DOMContentLoaded", (event) => {
    const parentElement = document.body;
    const canvas = document.createElement("canvas");
    canvas.id = "snow-canvas";
    canvas.height = "100%";
    canvas.width = "100%";
    attachCanvasOnElement(canvas, parentElement);

    // spawn snow on click
    parentElement.addEventListener("click", (event) => {
        for(let i = 0; i < CLICK_SNOW_SPAWN_COUNT; ++i) {
            snowArray.push(new Snow(event.x, event.y));
        }
    });

    // resize as parent resize
    addEventListener("resize", (event) => {
        canvas.width = parentElement.offsetWidth;
        canvas.height = parentElement.offsetHeight;
    });

    // init ticks
    const init_tick_round = canvas.height / MIN_SNOW_FORCE + MAX_SNOW_ALPHA / SNOW_MELT_RATE;
    for(let i = 0; i < init_tick_round; ++i) {
        tick(canvas.width, canvas.height);
    }

    // main loop
    setInterval(() => {
        tick(canvas.width, canvas.height);
        render(canvas);
    }, TICK_DURATION)
})
